import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message } from 'semantic-ui-react';
import Validator from 'validator';
import InlineError from '../messages/InlineError';

class LoginForm extends React.Component {
    state = {
        data: {
            email: '',
            password: ''
        },
        loading: false,
        errors: {}
    };


    onChange = e => 
        this.setState({
            data: { ...this.state.data, [e.target.name]: e.target.value }
            // 1) burda [e.target.name]: e.target.value - bunu yaratmaqda meqsed ->
            // -> iki defe changeEmail, changePassword yazmamaq ucun ve asagida ancaq change islenir ikisindede !
            // 2) burda ... change-den qabaq data save etmek ucundur deyesen sehf etmiremse.
        });


    onSubmit = () => {
        const errors = this.validate(this.state.data);
        this.setState({ errors });
        if (Object.keys(errors).length === 0) {
            this.setState({ loading: true });
            this.props.submit(this.state.data)
            .catch(err => this.setState({ errors: err.response.data.errors, loading: false }));
        }
    };

    validate = (data) => {
        const errors = {};
        if (!Validator.isEmail(data.email)) errors.email = "invalid email";
        if (!data.password) errors.password = "cant be blank";
        return errors;
    };

    render() {
        const { data, errors, loading } = this.state; // her yerde bunu yazmamaq ucun { this.state. ... } variable
        return (
            <Form onSubmit={this.onSubmit} loading={loading}>
                { errors.global && <Message negative>
                    <Message.Header>Something went wrong</Message.Header>
                    <p>{errors.global}</p>
                 </Message> }
                <Form.Field error={!!errors.email}>
                    <label htmlFor="email">email</label>
                    <input 
                        type="email" 
                        id="email" 
                        name="email" 
                        placeholder="exp@mail.ru"
                        value={ data.email }
                        onChange={this.onChange} />
                    {errors.email && <InlineError text={errors.email} />}
                </Form.Field>
                <Form.Field error={!!errors.password}>
                    <label htmlFor="password">password</label>
                    <input 
                        type="password" 
                        id="password" 
                        name="password" 
                        placeholder="Make it secure"
                        value={ data.password }
                        onChange={this.onChange} />
                    {errors.password && <InlineError text={errors.password} />}
                </Form.Field>
                <Button primary>login</Button>
            </Form>
        );
    }
}


LoginForm.propTypes = {
    submit: PropTypes.func.isRequired
};

export default LoginForm;